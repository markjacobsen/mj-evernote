# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick and dirty access to the Evernote API https://github.com/evernote/evernote-sdk-java/
* 1.0.0

### How do I get set up? ###

* Clone the repo
* To use, add the project as a Maven dependency
* See documentation at http://dev.evernote.com/doc/

### Contribution guidelines ###

* Happy to have help if you're so inclined. Please create a pull request.

### Who do I talk to? ###

* markjacobsen.net